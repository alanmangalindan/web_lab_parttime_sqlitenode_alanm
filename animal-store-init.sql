drop table if exists OrderDetails;
drop table if exists Orders;
drop table if exists Animals;
drop table if exists Users;

create table Animals (
    id integer not null primary key,
    name varchar(50),
    shortDesc text,
    longDesc text,
    imageName varchar(50),
    cost integer
);

create table Users (
    username varchar(50) not null primary key,
    password varchar(50)
);

create table Orders (
    id integer not null primary key autoincrement,
    username varchar(50),
    timestamp timestamp,
    totalCost integer,
    foreign key (username) references Users (username)
);

create table OrderDetails (
    orderId integer not null,
    animalId integer not null,
    count integer,
    primary key (orderId, animalId),
    foreign key (orderId) references Orders (id),
    foreign key (animalId) references Animals (id)
);

insert into Users values 
    ('andrew', '12345'),
    ('waleed', '67890'),
    ('dominic', 'passw0rd'),
    ('admin', 'admin');

insert into Animals values
    (0, 'Arctic Fox', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a consectetur nunc. Curabitur vel viverra nibh.', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. In a consectetur nunc. Curabitur vel viverra nibh. Praesent aliquam at massa vel interdum. Aenean eleifend porttitor posuere. Nam vel dignissim nisl, a aliquet lorem. Donec placerat leo et sapien fermentum, tempus maximus elit ultrices. Nulla tellus arcu, interdum sit amet arcu pretium, placerat bibendum lorem. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed suscipit blandit neque, ac porta arcu cursus non. Nulla sollicitudin vel elit nec congue.', 'arctic_fox.gif', 850),
    (1, 'Himalayan Pika', "Mauris commodo varius neque ac eleifend. Etiam vestibulum neque ligula, in dapibus nulla dapibus ac.", "Mauris commodo varius neque ac eleifend. Etiam vestibulum neque ligula, in dapibus nulla dapibus ac. Integer ac arcu ac tellus pulvinar viverra at eu tortor. Suspendisse aliquet commodo justo, vel hendrerit est molestie et. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed mauris enim, facilisis sed feugiat sed, venenatis non urna. Praesent gravida ex vel purus laoreet, eu molestie ex vulputate. Aliquam magna ligula, placerat quis ultricies a, eleifend vel nunc. Nam efficitur nisl sed lorem facilisis elementum. Phasellus mollis pharetra rhoncus. Cras facilisis fermentum felis, ac condimentum diam tincidunt sed. Vestibulum iaculis quis sapien a volutpat. Etiam placerat leo id laoreet ornare. Vivamus pretium fringilla lectus, non imperdiet dui pharetra volutpat. In hac habitasse platea dictumst. Donec erat ligula, interdum eu erat et, accumsan hendrerit lorem.", "himalayan_pika.gif", 75),
    (2, "Lynx", "Proin viverra magna commodo nulla dignissim eleifend. Aliquam eleifend nulla ut nisi suscipit, ac hendrerit tellus dignissim.", "Proin viverra magna commodo nulla dignissim eleifend. Aliquam eleifend nulla ut nisi suscipit, ac hendrerit tellus dignissim. Sed ac neque et diam placerat pulvinar id eu nibh. Mauris quis ipsum vitae ligula molestie varius. Duis quis maximus massa. Integer elit felis, aliquam sit amet vulputate in, suscipit sed velit. Sed eget neque eu diam faucibus imperdiet.", "lynx.gif",	675),
    (3, "Pallas Cat", "Ut auctor leo sit amet bibendum cursus. Donec consectetur tristique dui, vitae ultrices erat aliquam hendrerit.", "Ut auctor leo sit amet bibendum cursus. Donec consectetur tristique dui, vitae ultrices erat aliquam hendrerit. Phasellus quis sollicitudin turpis. Donec mi urna, tempus tincidunt sodales ac, maximus eu quam. Mauris quis lobortis libero, in tempor tellus. Nullam vestibulum eleifend ornare. Sed at tortor nec diam ultricies sodales quis a risus. Aenean euismod nunc in pretium mattis. Aenean vel libero id lacus placerat vestibulum sed sit amet velit. Sed in malesuada eros, nec vulputate tellus. Etiam aliquet nunc at enim ultrices tincidunt. Sed imperdiet, sapien eget sodales viverra, augue massa vehicula nisl, sed ultricies sem tortor id enim.", "pallas_cat.gif", 350),
    (4, "Pika", "Quisque sit amet dapibus libero, feugiat consequat ligula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas.",	"Quisque sit amet dapibus libero, feugiat consequat ligula. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nullam lacinia eu nunc ut sollicitudin. Nunc finibus congue congue. Nulla eu posuere quam. Aenean non commodo felis. Morbi quis faucibus odio.", "pika.gif", 65),
    (5, "Quokka", "Nam lacus turpis, volutpat vitae magna sit amet, feugiat aliquet ipsum. In id libero elementum, vulputate nunc eget, pulvinar leo.", "Nam lacus turpis, volutpat vitae magna sit amet, feugiat aliquet ipsum. In id libero elementum, vulputate nunc eget, pulvinar leo. Donec vitae commodo justo. Mauris eu sem sem. Proin vehicula sagittis luctus. Cras dignissim dolor eu tellus congue pharetra id ac nisi. Aliquam a odio sollicitudin, blandit est at, accumsan augue. Nam cursus eleifend ipsum eu cursus. Integer augue orci, pellentesque ut malesuada nec, maximus non sapien.", "quokka.gif", 190),
    (6,	"Red Panda", "Praesent non feugiat mauris. Quisque sit amet massa ultricies, tempor libero vitae, tempus nunc.",	"Praesent non feugiat mauris. Quisque sit amet massa ultricies, tempor libero vitae, tempus nunc. Praesent sed leo a tellus rhoncus suscipit. Phasellus vestibulum gravida justo a accumsan. Nunc congue scelerisque commodo. Aenean velit turpis, auctor quis nisl vel, sodales blandit tortor. In at lorem et mauris ullamcorper imperdiet. Curabitur vel metus eu justo luctus vehicula. Nullam vehicula in urna in porta. Aenean tempus consequat dolor, non consequat tellus suscipit a. Praesent ac pellentesque felis.", "red_panda.gif", 1580),
    (7,	"Wombat", "Cras massa ex, pharetra at purus ut, pharetra dignissim nisi. Fusce sollicitudin arcu sed nibh tempus pretium.",	"Cras massa ex, pharetra at purus ut, pharetra dignissim nisi. Fusce sollicitudin arcu sed nibh tempus pretium. Phasellus pretium massa vitae libero posuere, at volutpat urna elementum. Curabitur vitae placerat metus, sed malesuada justo. Aenean at magna gravida, laoreet nulla sed, molestie sem. Suspendisse potenti. Nulla vel metus facilisis, auctor lacus sed, imperdiet massa. Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin sit amet aliquam dui. Curabitur eget nisi sit amet neque volutpat dapibus ac a arcu. Donec urna turpis, bibendum nec vehicula vel, hendrerit vel diam. Nulla at risus ut metus porttitor ornare. Etiam ac nibh tempor, lacinia lacus eu, dignissim orci. Donec aliquam felis a massa ullamcorper vulputate.", "wombat.gif",	300),
    (8,	"T-Rex", "Highly dangerous. Reccommended for expert users only! Aenean auctor orci sed lorem maximus ullamcorper.", "Highly dangerous. Reccommended for expert users only! Aenean auctor orci sed lorem maximus ullamcorper. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean at velit ut lectus porta elementum at id justo. Integer euismod elit facilisis ipsum sodales, vitae commodo odio placerat. Aliquam lobortis, nulla volutpat condimentum blandit, turpis nisl pellentesque metus, vel commodo massa dui in metus. Nam ultrices massa non est ultrices, a congue arcu facilisis. Morbi et purus ut diam lacinia mollis ut lobortis dui.", "t-rex.png", 25000);

insert into Orders values
    (0, "admin", "2017-10-09 15:51", 46625),
    (1, "admin", "2017-10-09 15:49", 275580),
    (2, "admin", "2017-10-09 15:45", 4945),
    (3, "andrew", "2017-10-09 15:48", 2500000),
    (4, "andrew", "2017-11-01 13:01", 1600),
    (5, "andrew", "2017-11-01 13:37", 1400375);

insert into OrderDetails values
    (0, 0, 47),
    (0, 1, 89),
    (1, 2, 18),
    (1, 6, 7),
    (1, 8, 7),
    (1, 0, 65),
    (2, 1, 7),
    (2, 2, 5),
    (2, 4, 3),
    (2, 0, 1),
    (3, 8, 100),
    (4, 0, 1),
    (4, 1, 1),
    (4, 2, 1),
    (5, 1, 1),
    (5, 7, 1),
    (5, 8, 56);

