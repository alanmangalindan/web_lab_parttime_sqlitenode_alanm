var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('animal-store-lab.db');

var dateTime = require('node-datetime');

module.exports.getUser = function (username, callback) {
    db.all("select * from Users where username = ?", [username], function (err, rows) {
        if (rows.length > 0) {
            callback(rows[0]);
        } else {
            callback(null);
        }
    });
}

module.exports.getAllAnimals = function (callback) {
    db.all("select * from Animals", function (err, rows) {
        callback(rows);
    });
}

module.exports.getOrderHistoryFor = function (username, callback) {
    db.all("select * from Orders where username = ? order by timestamp desc", [username], function (err, orderRows) {
        var orderHistoryMap = {};

        for (var i = 0; i < orderRows.length; i++) {
            orderHistoryMap[orderRows[i].id] = orderRows[i];
        }

        db.all("SELECT od.animalId as 'animalId', a.name as 'name', a.imageName as 'imageName', od.orderId as 'orderId', od.count as 'count' " +
            "FROM Orders o, OrderDetails od, Animals a " +
            "WHERE o.username = ? AND o.id = od.orderId AND a.id = od.animalId", [username], function (err, orderDetailRows) {

                if (err) {
                    console.log(err);
                }

                for (var i = 0; i < orderDetailRows.length; i++) {
                    var orderDetailRow = orderDetailRows[i];

                    if (!orderHistoryMap[orderDetailRow.orderId].details) {
                        orderHistoryMap[orderDetailRow.orderId].details = [];
                    }

                    orderHistoryMap[orderDetailRow.orderId].details.push({
                        animalId: orderDetailRow.animalId,
                        animalName: orderDetailRow.name,
                        imageName: orderDetailRow.imageName,
                        count: orderDetailRow.count
                    });
                }

                console.log(orderRows);

                callback(orderRows);
            });
    });
}

module.exports.saveOrderFor = function (username, order, callback) {

    var date = dateTime.create();
    var timestamp = date.format("Y-m-d H:M");

    db.run("insert into Orders (username, timestamp, totalCost) values (?, ?, ?)", [username, timestamp, order.totalCost], function (err) {
        var orderId = this.lastID;

        // Everything in here will be run one after the other, without interfering with each other incorrectly.
        db.serialize(function () {

            db.run("BEGIN TRANSACTION");

            for (var i = 0; i < order.items.length; i++) {
                var orderLine = order.items[i];

                db.run("INSERT INTO OrderDetails VALUES (?, ?, ?)", [orderId, orderLine.id, orderLine.count]);
            }

            db.run("COMMIT");

        });

        callback();
    });
}